package src.java.homework05;

import java.util.Scanner;

public class homework05 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int number = scn.nextInt();
        int min = 0;
        do {
            number = scn.nextInt();

            int digit1 = number / 100;
            int digit2 = (number % 100)/10;
            int digit3 = number % 10;

            if (digit1 < digit2 && digit1 < digit3 && digit1 > -1){
                min = digit1;
            }
            if (digit2 < digit1 && digit2 < digit3  && digit2 > -1){
                min = digit2;
            }
            if (digit3 < digit1 && digit3 < digit2 && digit3 > -1){
                min = digit3;
            }

        }while (number != -1);
        System.out.println(min);

    }
}